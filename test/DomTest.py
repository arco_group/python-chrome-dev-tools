# -*- mode: python; coding: utf-8 -*-

from unittest import TestCase
from hamcrest import is_, instance_of
from doublex import Stub, assert_that

from chrome import Node, DOM


class CommandTester(TestCase):
    def setUp(self):
        self.wsock = Stub()
        self.dom = DOM(self.wsock)

    def test_getDocument_sends_correct_message(self):
        node = self.dom.getDocument()
        assert_that(self.wsock,

    def test_getDocument_returns_Node(self):
        node = self.dom.getDocument()
        assert_that(node, is_(instance_of(Node)))
